package es

import (
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/is"
	"gitlab.com/ljpcore/golib/uuid"
)

func TestBaseAggregateStreamID(t *testing.T) {
	// Arrange.
	StreamID1 := uuid.New()
	a := &BaseAggregate{}

	// Pre-condition.
	StreamID2 := a.GetStreamID()
	test.That(t, StreamID2, is.EqualTo(uuid.Empty))

	// Act.
	a.SetStreamID(StreamID1)
	StreamID3 := a.GetStreamID()

	// Assert.
	test.That(t, StreamID3, is.EqualTo(StreamID1))
}

func TestBaseAggregateSequenceNumber(t *testing.T) {
	// Arrange.
	sequenceNumber1 := 4
	a := &BaseAggregate{}

	// Pre-condition.
	sequenceNumber2 := a.GetSequenceNumber()
	test.That(t, sequenceNumber2, is.EqualTo(0))

	// Act.
	a.SetSequenceNumber(sequenceNumber1)
	sequenceNumber3 := a.GetSequenceNumber()

	// Assert.
	test.That(t, sequenceNumber3, is.EqualTo(sequenceNumber1))
}

func TestBaseAggregateAddPendingEvent(t *testing.T) {
	// Arrange.
	streamID := uuid.New()
	a := &BaseAggregate{streamID: streamID}
	a.AddPendingEvent(&InvoiceCreatedEvent{Amount: 200})
	a.ClearPendingEvents()

	e := &InvoicePaymentSucceededEvent{
		Amount: 100,
	}

	// Act.
	a.AddPendingEvent(e)

	// Assert.
	test.That(t, e.Amount, is.EqualTo(int64(100)))
	test.That(t, e.StreamID, is.EqualTo(streamID))
	test.That(t, e.EventID, is.NotEqualTo(uuid.Empty))
	test.That(t, e.SequenceNumber, is.EqualTo(1))
	test.That(t, a.sequenceNumber, is.EqualTo(2))
	test.That(t, len(a.pendingEvents), is.EqualTo(1))
}
