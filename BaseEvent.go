package es

import "gitlab.com/ljpcore/golib/uuid"

// BaseEvent implements the basic getter/setter functionality of the Event
// interface.  It is not required to be used, but removes a lot of boilerplate
// code that would otherwise require implementation on all concrete event types.
// Prefer embedding as BaseEvent rather than *BaseEvent.  Does not implement the
// Name method - all event types must be manually named.
type BaseEvent struct {
	EventID        uuid.UUID `json:"eventID"`
	StreamID       uuid.UUID `json:"streamID"`
	SequenceNumber int       `json:"sequenceNumber"`
}

// GetEventID returns the ID of the event itself.
func (e *BaseEvent) GetEventID() uuid.UUID {
	return e.EventID
}

// SetEventID sets the ID of the event itself.
func (e *BaseEvent) SetEventID(id uuid.UUID) {
	e.EventID = id
}

// GetStreamID returns the ID of the stream that the event belongs to.
func (e *BaseEvent) GetStreamID() uuid.UUID {
	return e.StreamID
}

// SetStreamID sets the ID of the stream itself.
func (e *BaseEvent) SetStreamID(id uuid.UUID) {
	e.StreamID = id
}

// GetSequenceNumber returns the location of the event in the sequence of events
// belonging to the stream.
func (e *BaseEvent) GetSequenceNumber() int {
	return e.SequenceNumber
}

// SetSequenceNumber sets the location of the event in the sequence of events
// belonging to the stream.
func (e *BaseEvent) SetSequenceNumber(n int) {
	e.SequenceNumber = n
}
