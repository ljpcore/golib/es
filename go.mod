module gitlab.com/ljpcore/golib/es

go 1.15

require (
	gitlab.com/ljpcore/golib/test v0.1.2
	gitlab.com/ljpcore/golib/uuid v0.1.1
)
