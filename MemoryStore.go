package es

import (
	"sort"

	"gitlab.com/ljpcore/golib/uuid"
)

// MemoryStore is an implementation of Store that stores event streams in
// memory.  It should be used only for unit testing.
type MemoryStore struct {
	events map[uuid.UUID][]Event
}

var _ Store = &MemoryStore{}

// NewMemoryStore returns an empty, ready-to-use MemoryStore.
func NewMemoryStore() *MemoryStore {
	return &MemoryStore{
		events: map[uuid.UUID][]Event{},
	}
}

// ReadFromStream reads the set of events for the provided streamID, if it
// exists.
func (s *MemoryStore) ReadFromStream(streamID uuid.UUID) ([]Event, error) {
	events, ok := s.events[streamID]
	if !ok {
		return nil, StreamNotFoundError(streamID)
	}

	return events, nil
}

// ReadToAggregate reads the set of events for the provided streamID, if it
// exists, and then applies them to the provided aggregate.
func (s *MemoryStore) ReadToAggregate(streamID uuid.UUID, aggregate Aggregate) error {
	if aggregate.GetSequenceNumber() != 0 {
		return AggregateSequenceNumberNonZeroError(aggregate.GetStreamID())
	}

	events, err := s.ReadFromStream(streamID)
	if err != nil {
		return err
	}

	aggregate.SetStreamID(streamID)
	for _, event := range events {
		aggregate.Apply(event)
	}

	return nil
}

// WriteToStream commits the events provided to storage.
func (s *MemoryStore) WriteToStream(events []Event) error {
	if len(events) < 1 {
		return nil
	}

	streamID, err := s.ensureValidEventSequencing(events)
	if err != nil {
		return err
	}

	x, _ := s.events[streamID]
	x = append(x, events...)
	s.events[streamID] = x

	return nil
}

// WriteFromAggregate commits the events from the provided aggregate to
// storage.
func (s *MemoryStore) WriteFromAggregate(aggregate Aggregate) error {
	events := aggregate.GetPendingEvents()
	if err := s.WriteToStream(events); err != nil {
		return err
	}

	aggregate.ClearPendingEvents()
	return nil
}

func (s *MemoryStore) ensureValidEventSequencing(events []Event) (uuid.UUID, error) {
	expectedStreamID := events[0].GetStreamID()
	for _, event := range events {
		if event.GetStreamID() != expectedStreamID {
			return uuid.Empty, ConflictingStreamIDError(expectedStreamID, event.GetStreamID())
		}
	}

	orderedEvents := make([]Event, len(events))
	copy(orderedEvents, events)
	sort.Slice(orderedEvents, func(i, j int) bool {
		return events[i].GetSequenceNumber() < events[j].GetSequenceNumber()
	})

	expectedSequenceNumber := 0
	existingEvents, ok := s.events[expectedStreamID]
	if ok && len(existingEvents) > 0 {
		expectedSequenceNumber = len(existingEvents)
	}

	for _, event := range orderedEvents {
		if event.GetSequenceNumber() != expectedSequenceNumber {
			return uuid.Empty, ConcurrentSequencingError(expectedStreamID, expectedSequenceNumber, event.GetSequenceNumber())
		}

		expectedSequenceNumber++
	}

	return expectedStreamID, nil
}
