package es

import (
	"fmt"

	"gitlab.com/ljpcore/golib/uuid"
)

// Store defines the methods that any type capable of storing and retrieving
// events must implement.
//
// This package provides a single implementation, MemoryStore.  Production
// implementations of this interface are not included in the package.
type Store interface {
	ReadFromStream(streamID uuid.UUID) ([]Event, error)
	ReadToAggregate(streamID uuid.UUID, aggregate Aggregate) error

	WriteToStream(events []Event) error
	WriteFromAggregate(aggregate Aggregate) error
}

// StreamNotFoundError describes an error scenario where an event stream is
// requested but does not exist in the store.
func StreamNotFoundError(streamID uuid.UUID) error {
	return fmt.Errorf("a stream with ID '%v' does not exist", streamID)
}

// AggregateSequenceNumberNonZeroError describes an error scenario where an
// aggregate has a non-zero sequence number where it was expected to have had no
// events applied.
func AggregateSequenceNumberNonZeroError(streamID uuid.UUID) error {
	return fmt.Errorf("the aggregate with ID '%v' has a non-zero sequence number, so cannot be loaded into from a store", streamID)
}

// ConcurrentSequencingError describes an error scenario where events are
// attempted to be written to a stream that do not respect the sequencing of the
// events currently in that stream.
func ConcurrentSequencingError(streamID uuid.UUID, expectedSequenceNumber, actualSequenceNumber int) error {
	return fmt.Errorf("expected next sequence number for stream '%v' to be %v, but was %v", streamID, expectedSequenceNumber, actualSequenceNumber)
}

// ConflictingStreamIDError describes an error scenario where a set of events
// are being written to a stream but they have different streamIDs.
func ConflictingStreamIDError(streamID1 uuid.UUID, streamID2 uuid.UUID) error {
	return fmt.Errorf("expected all streamIDs in event set to be the same, but at least two were present: '%v' and '%v'", streamID1, streamID2)
}
