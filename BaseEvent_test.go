package es

import (
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/is"
	"gitlab.com/ljpcore/golib/uuid"
)

func TestBaseEventEventID(t *testing.T) {
	// Arrange.
	eventID1 := uuid.New()
	base := &BaseEvent{}

	// Pre-condition.
	eventID2 := base.GetEventID()
	test.That(t, eventID2, is.EqualTo(uuid.Empty))

	// Act.
	base.SetEventID(eventID1)
	eventID3 := base.GetEventID()

	// Assert.
	test.That(t, eventID3, is.EqualTo(eventID1))
}

func TestBaseEventStreamID(t *testing.T) {
	// Arrange.
	StreamID1 := uuid.New()
	base := &BaseEvent{}

	// Pre-condition.
	StreamID2 := base.GetStreamID()
	test.That(t, StreamID2, is.EqualTo(uuid.Empty))

	// Act.
	base.SetStreamID(StreamID1)
	StreamID3 := base.GetStreamID()

	// Assert.
	test.That(t, StreamID3, is.EqualTo(StreamID1))
}

func TestBaseEventSequenceNumber(t *testing.T) {
	// Arrange.
	sequenceNumber1 := 4
	base := &BaseEvent{}

	// Pre-condition.
	sequenceNumber2 := base.GetSequenceNumber()
	test.That(t, sequenceNumber2, is.EqualTo(0))

	// Act.
	base.SetSequenceNumber(sequenceNumber1)
	sequenceNumber3 := base.GetSequenceNumber()

	// Assert.
	test.That(t, sequenceNumber3, is.EqualTo(sequenceNumber1))
}
