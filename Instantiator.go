package es

import (
	"fmt"
	"reflect"
	"sync"
)

// Instantiator is used to create empty instances of different event types by
// their event name.  It is intended to be used by implementations of Store that
// require deserialization of events into concrete types.  It is safe for
// concurrent use.
type Instantiator struct {
	types map[string]reflect.Type
	mx    *sync.RWMutex
}

// NewInstantiator creates a new, empty Instantiator.
func NewInstantiator() *Instantiator {
	return &Instantiator{
		types: map[string]reflect.Type{},
		mx:    &sync.RWMutex{},
	}
}

// AddEventType adds an event type to the Instantiator with the provided name.
// This call expects event to be a pointer to a struct.  The call will panic if
// this is not the case.
func (i *Instantiator) AddEventType(event Event) {
	t := reflect.TypeOf(event)
	if t.Kind() != reflect.Ptr || t.Elem().Kind() != reflect.Struct {
		panic(fmt.Sprintf("expected event type to be pointer to a struct, but was %v", t))
	}

	i.mx.Lock()
	defer i.mx.Unlock()

	i.types[event.Name()] = t.Elem()
}

// NewEventOfType produces a new, empty event of the provided type.
func (i *Instantiator) NewEventOfType(name string) (Event, error) {
	t := func() reflect.Type {
		i.mx.RLock()
		defer i.mx.RUnlock()

		t, _ := i.types[name]
		return t
	}()

	if t == nil {
		return nil, fmt.Errorf("could not instantiate event: unknown event name '%v'", name)
	}

	return reflect.New(t).Interface().(Event), nil
}
