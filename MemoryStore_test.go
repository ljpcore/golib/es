package es

import (
	"fmt"
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/has"
	"gitlab.com/ljpcore/golib/test/is"
	"gitlab.com/ljpcore/golib/uuid"
)

func TestMemoryStoreWriteZeroEvents(t *testing.T) {
	testCases := [][]Event{
		[]Event{},
		nil,
	}

	for _, testCase := range testCases {
		// Arrange.
		streamID := uuid.New()
		ms := NewMemoryStore()

		// Act.
		err := ms.WriteToStream(testCase)
		test.That(t, err, is.Nil)

		// Assert.
		events, err := ms.ReadFromStream(streamID)
		test.That(t, err, is.NotNil)
		test.That(t, events, has.Length(0))
	}
}

func TestMemoryStoreReadWriteEvents(t *testing.T) {
	// Arrange.
	eventID := uuid.New()
	streamID := uuid.New()
	e := &InvoiceCreatedEvent{
		BaseEvent: BaseEvent{
			EventID:        eventID,
			StreamID:       streamID,
			SequenceNumber: 0,
		},
	}

	events1 := []Event{e}
	ms := NewMemoryStore()

	// Act.
	err := ms.WriteToStream(events1)
	test.That(t, err, is.Nil)

	events2, err := ms.ReadFromStream(streamID)

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, events2, has.Length(1))
	test.That(t, events2[0].GetEventID(), is.EqualTo(eventID))
	test.That(t, events2[0].GetStreamID(), is.EqualTo(streamID))
	test.That(t, events2[0].GetSequenceNumber(), is.EqualTo(0))
}

func TestMemoryStoreFailsConcurrentSequencing(t *testing.T) {
	// Arrange.
	streamID := uuid.New()
	initialEvents := []Event{
		&InvoiceCreatedEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 0,
			},
		},
		&InvoicePaymentSucceededEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 1,
			},
		},
	}

	ms := NewMemoryStore()
	err := ms.WriteToStream(initialEvents)
	test.That(t, err, is.Nil)

	// Act.
	err = ms.WriteToStream([]Event{
		&InvoicePaymentSucceededEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 3,
			},
		},
	})

	// Assert.
	test.That(t, err, is.NotNil)
	expectedError := fmt.Sprintf("expected next sequence number for stream '%v' to be 2, but was 3", streamID)
	test.That(t, err.Error(), is.EqualTo(expectedError))
}

func TestMemoryStoreDifferentStreamIDsInEventSet(t *testing.T) {
	// Arrange.
	ms := NewMemoryStore()
	events := []Event{
		&InvoiceCreatedEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       uuid.New(),
				SequenceNumber: 0,
			},
		},
		&InvoicePaymentSucceededEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       uuid.New(),
				SequenceNumber: 1,
			},
		},
	}

	// Act.
	err := ms.WriteToStream(events)

	// Assert.
	test.That(t, err, is.NotNil)
}

func TestMemoryStoreFailsConcurrentSequencingForSkipping(t *testing.T) {
	// Arrange.
	streamID := uuid.New()
	initialEvents := []Event{
		&InvoiceCreatedEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 0,
			},
		},
		&InvoicePaymentSucceededEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 1,
			},
		},
	}

	ms := NewMemoryStore()
	err := ms.WriteToStream(initialEvents)
	test.That(t, err, is.Nil)

	// Act.
	err = ms.WriteToStream([]Event{
		&InvoicePaymentSucceededEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 2,
			},
		},
		&InvoicePaymentSucceededEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 4,
			},
		},
	})

	// Assert.
	test.That(t, err, is.NotNil)
	expectedError := fmt.Sprintf("expected next sequence number for stream '%v' to be 3, but was 4", streamID)
	test.That(t, err.Error(), is.EqualTo(expectedError))
}

func TestMemoryStorePassesConcurrentSequencing(t *testing.T) {
	// Arrange.
	streamID := uuid.New()
	initialEvents := []Event{
		&InvoiceCreatedEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 0,
			},
		},
		&InvoicePaymentSucceededEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 1,
			},
		},
	}

	ms := NewMemoryStore()
	err := ms.WriteToStream(initialEvents)
	test.That(t, err, is.Nil)

	// Act.
	err = ms.WriteToStream([]Event{
		&InvoicePaymentSucceededEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 3,
			},
		},
		&InvoicePaymentSucceededEvent{
			BaseEvent: BaseEvent{
				EventID:        uuid.New(),
				StreamID:       streamID,
				SequenceNumber: 2,
			},
		},
	})

	// Assert.
	test.That(t, err, is.Nil)
}

func TestMemoryStoreReadWriteAggregate(t *testing.T) {
	// Arrange.
	ms := NewMemoryStore()
	a1 := NewInvoice(100)

	err := a1.FeeRaised(25)
	test.That(t, err, is.Nil)

	err = a1.PaymentSucceeded(50)
	test.That(t, err, is.Nil)

	// Pre-condition.
	test.That(t, a1.BalanceOwing(), is.EqualTo(int64(75)))
	test.That(t, len(a1.GetPendingEvents()), is.EqualTo(3))

	// Act.
	err = ms.WriteFromAggregate(a1)
	test.That(t, err, is.Nil)

	a2 := &Invoice{}
	err = ms.ReadToAggregate(a1.GetStreamID(), a2)

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, a2.BalanceOwing(), is.EqualTo(int64(75)))
	test.That(t, len(a2.GetPendingEvents()), is.EqualTo(0))
}
