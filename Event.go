package es

import "gitlab.com/ljpcore/golib/uuid"

// An Event has, at bare minimum, an EventID, StreamID and Sequence Number.
// This interface ensures all events can be retrieved and mutated by the other
// types in this package.
//
// It is recommended that the BaseEvent type is embedded into concrete event
// types to automatically satisfy this interface.
//
// The EventID is a unique identifier for the event itself.  The StreamID is the
// identifier of the event stream, and the Sequence Number is the location of
// the event in that stream.
type Event interface {
	Name() string

	GetEventID() uuid.UUID
	SetEventID(id uuid.UUID)

	GetStreamID() uuid.UUID
	SetStreamID(id uuid.UUID)

	GetSequenceNumber() int
	SetSequenceNumber(n int)
}
