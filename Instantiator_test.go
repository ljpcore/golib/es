package es

import (
	"encoding/json"
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/is"
)

func TestInstantiatorKnownEvent(t *testing.T) {
	// Arrange.
	i := NewInstantiator()
	jsonStr := []byte(`{"amount":122}`)

	i.AddEventType(&BillCreatedEvent{})

	// Act.
	e, err := i.NewEventOfType("BillCreated")
	test.That(t, err, is.Nil)

	err = json.Unmarshal(jsonStr, e)
	test.That(t, err, is.Nil)

	// Assert.
	invoiceCreatedEvent, ok := e.(*BillCreatedEvent)
	test.That(t, ok, is.True)
	test.That(t, invoiceCreatedEvent.Amount, is.EqualTo(int64(122)))
}

func TestInstantiatorUnknownEvent(t *testing.T) {
	// Arrange.
	i := NewInstantiator()

	// Act.
	e, err := i.NewEventOfType("BillCreated")

	// Assert.
	test.That(t, err, is.NotNil)
	test.That(t, err.Error(), is.EqualTo("could not instantiate event: unknown event name 'BillCreated'"))
	test.That(t, e, is.Nil)
}

type BillCreatedEvent struct {
	BaseEvent
	Amount int64
}

func (e *BillCreatedEvent) Name() string {
	return "BillCreated"
}
