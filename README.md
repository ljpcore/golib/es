![](icon.png)

# es

An abstract set of interfaces that enable sensible event sourcing.

---

Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from
[www.flaticon.com](https://www.flaticon.com/).
