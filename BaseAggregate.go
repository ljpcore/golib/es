package es

import "gitlab.com/ljpcore/golib/uuid"

// BaseAggregate partially implements the Aggregate interface.  It is intended
// to be embedded into concrete Aggregate types to provide the core
// functionality of an aggregate.  Only the Apply method of the Aggregate
// interface is left unimplemented.
type BaseAggregate struct {
	streamID       uuid.UUID
	sequenceNumber int
	pendingEvents  []Event
}

// GetStreamID returns the ID of the stream that the events in the aggregate
// belong to.
func (a *BaseAggregate) GetStreamID() uuid.UUID {
	return a.streamID
}

// SetStreamID sets the ID of the stream that the events in the aggregate
// belong to.
func (a *BaseAggregate) SetStreamID(id uuid.UUID) {
	a.streamID = id
}

// GetSequenceNumber returns the sequence number that will be assigned to the
// next event added to the pending events for the aggregate.
func (a *BaseAggregate) GetSequenceNumber() int {
	return a.sequenceNumber
}

// SetSequenceNumber sets the sequence number that will be assigned to the next
// event added to the pending events for the aggregate.
func (a *BaseAggregate) SetSequenceNumber(n int) {
	a.sequenceNumber = n
}

// GetPendingEvents returns the set of events that have been applied to the
// aggregate but not yet committed to a store.
func (a *BaseAggregate) GetPendingEvents() []Event {
	return a.pendingEvents
}

// AddPendingEvent adds an Event to the set of pending events for this
// aggregate.
func (a *BaseAggregate) AddPendingEvent(e Event) {
	e.SetEventID(uuid.New())
	e.SetStreamID(a.GetStreamID())
	e.SetSequenceNumber(a.sequenceNumber)

	a.sequenceNumber++
	a.pendingEvents = append(a.pendingEvents, e)
}

// ClearPendingEvents empties the set of pending events for this aggregate.
func (a *BaseAggregate) ClearPendingEvents() {
	a.pendingEvents = nil
}
