package es

import "gitlab.com/ljpcore/golib/uuid"

// An Aggregate is a domain object that contains business logic.  It determines
// how the state of a concrete type changes as events are applied to it.
//
// It is recommended that the BaseAggregate type is embedded into concrete
// aggregate types to partially satisfy this interface.  If BaseAggregate is
// embedded, only the Apply method will need to be implemented manually.
type Aggregate interface {
	GetStreamID() uuid.UUID
	SetStreamID(streamID uuid.UUID)

	GetSequenceNumber() int
	SetSequenceNumber(n int)

	GetPendingEvents() []Event
	AddPendingEvent(e Event)
	ClearPendingEvents()

	Apply(e Event)
}
